lint:
	go vet ./...

tests:
	go test ./...

build:
	docker-compose build

run:
	docker-compose up

stop:
	docker-compose stop